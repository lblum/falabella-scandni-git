﻿using System;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Data.Odbc;
using System.IO;

namespace AperturaEnCaja
{
    class PDFFile
    {
        private Document pdfDoc = new Document(PageSize.A4);

        private iTextSharp.text.Font _standardFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.HELVETICA, 12, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);

        public PDFFile()
        {

        }

        protected void addDataLine(PdfPTable tbl, string label, string texto)
        {
            tbl.AddCell(
                new PdfPCell
                {
                    Phrase = new Phrase(label, _standardFont),
                    HorizontalAlignment = Element.ALIGN_RIGHT
                }
                );
            tbl.AddCell(
                new PdfPCell
                {
                    Phrase = new Phrase(texto, _standardFont),
                    HorizontalAlignment = Element.ALIGN_LEFT
                }
                );

        }

        protected void addData(OdbcDataReader rdr, PdfPTable tbl)
        {
            PdfPTable data = new PdfPTable(2);
            addDataLine(data, "Caja", rdr["id_caja"].ToString());
            addDataLine(data, "Cajero", rdr["username"].ToString());
            string dtStr = rdr["ts"].ToString();
            DateTime dt = DateTime.Parse(dtStr);
            addDataLine(data, "Generado el ", dt.ToString("dd/MM/yyyy HH:mm:ss"));
            addDataLine(data, "DNI nro.", rdr["dni"].ToString());
            addDataLine(data, "Apellido(s)", rdr["apellidos"].ToString());
            addDataLine(data, "Nombre(s)", rdr["nombres"].ToString());
            string txtSexo = "Femenino";
            if (rdr["sexo"].ToString().Length > 0)
            {
                if (rdr["sexo"].ToString().Length == 1)
                {
                    txtSexo = rdr["sexo"].ToString() != "F" ? "Masculino" : "Femenino" ;
                } else
                {
                    txtSexo = rdr["sexo"].ToString();
                }
            }


            addDataLine(data, "Sexo", txtSexo);

            addDataLine(data, "Nacionalidad", rdr["nacionalidad"].ToString());
            try
            {
                dtStr = rdr["fnacimiento"].ToString();
                dt = DateTime.Parse(dtStr);
                addDataLine(data, "Fecha de nacimiento", dt.ToString("dd/MM/yyyy"));
            } catch(Exception ex)
            {

                addDataLine(data, "Fecha de nacimiento", "01/01/2000");
            }
            addDataLine(data, "Celular", rdr["celular"].ToString());
            addDataLine(data, "Email", rdr["email"].ToString());
            addDataLine(data, "Importe de la compra", rdr["importe"].ToString());
            //addDataLine(data, "PAN o resultado de la operación", rdr["ccnum"].ToString());
            //addDataLine(data, Chunk., "");


            tbl.AddCell(new PdfPCell(data));

        }

        protected void addImgData(OdbcDataReader rdr, PdfPTable tbl)
        {
            PdfPTable data = new PdfPTable(1);

            byte[] imgData;
            Image img;
            imgData = (byte[])rdr["img1"];
            img = Image.GetInstance(Decoder.fromByteArray(imgData), System.Drawing.Imaging.ImageFormat.Bmp);
            img.ScaleToFit(200, 200);
            data.AddCell(new PdfPCell(img));

            imgData = (byte[])rdr["img2"];
            img = Image.GetInstance(Decoder.fromByteArray(imgData), System.Drawing.Imaging.ImageFormat.Bmp);
            img.ScaleToFit(200, 200);
            data.AddCell(new PdfPCell(img));

            tbl.AddCell(data);

        }


        public void createFile(string fileName, OdbcDataReader rdr)
        {
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(fileName, FileMode.Create));
            pdfDoc.Open();

            PdfPTable tblResumen = new PdfPTable(4);
            tblResumen.WidthPercentage = 100;
            PdfPCell clCaja = new PdfPCell(new Phrase("Caja", _standardFont));
            clCaja.BorderWidth = 0;
            clCaja.BorderWidthBottom = 0.75f;

            PdfPCell clUser = new PdfPCell(new Phrase("Cajero", _standardFont));
            clUser.BorderWidth = 0;
            clUser.BorderWidthBottom = 0.75f;

            PdfPCell clDNI = new PdfPCell(new Phrase("DNI nro", _standardFont));
            clDNI.BorderWidth = 0;
            clDNI.BorderWidthBottom = 0.75f;

            PdfPCell clNombre = new PdfPCell(new Phrase("Apellido(s) y nombre(s)", _standardFont));
            clNombre.BorderWidth = 0;
            clNombre.BorderWidthBottom = 0.75f;

            tblResumen.AddCell(clCaja);
            tblResumen.AddCell(clUser);
            tblResumen.AddCell(clDNI);
            tblResumen.AddCell(clNombre);
            int nPages = 0;
            while (rdr.Read())
            {
                // Solo los que tengan PAN
                string pan = rdr["ccnum"].ToString();
                long n;
               // if (!long.TryParse(pan, out n))
                //    continue;
                nPages++;
                clCaja = new PdfPCell(new Phrase(rdr["id_caja"].ToString(), _standardFont));
                clCaja.BorderWidth = 0;
                clUser = new PdfPCell(new Phrase(rdr["username"].ToString(), _standardFont));
                clUser.BorderWidth = 0;
                clDNI = new PdfPCell(new Phrase(rdr["dni"].ToString(), _standardFont));
                clDNI.BorderWidth = 0;
                clNombre = new PdfPCell(new Phrase(rdr["apellidos"].ToString() + "," + rdr["nombres"].ToString(), _standardFont));
                clNombre.BorderWidth = 0;

                tblResumen.AddCell(clCaja);
                tblResumen.AddCell(clUser);
                tblResumen.AddCell(clDNI);
                tblResumen.AddCell(clNombre);
                PdfPTable tblDetalle = new PdfPTable(2);
                addData(rdr, tblDetalle);
                addImgData(rdr, tblDetalle);
                pdfDoc.Add(tblDetalle);
                pdfDoc.NewPage();
            }

            //pdfDoc.NewPage();
            //pdfDoc.Add(tblResumen);

            if ( nPages == 0)
            {
                rdr.Close();
                try
                {
                    rdr.Close();
                    pdfDoc.Close();
                    writer.Close();
                } catch
                {

                }
                throw new Exception("No hay datos con esos filtros");
            }



            rdr.Close();
            pdfDoc.Close();
            writer.Close();


        }

    }

}
