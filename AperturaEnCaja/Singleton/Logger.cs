﻿using System;
using System.Collections.Generic;
using log4net;
using log4net.Config;

namespace AperturaEnCaja.Singleton
{
    public static class Logger
    {
        #region Members
        private static readonly ILog logger = LogManager.GetLogger(typeof(Logger));
        private static readonly Dictionary<int, string> customMsgs = new Dictionary<int, string>();
        
        static readonly log4net.Core.Level traceLevel = new log4net.Core.Level(50000, "TRACE");

        private enum customCodes : int
        {
            NONE = -1, // Antes de comenzar
            APP = 0,// Comienzo y fin de la aplicación
            CLIENT = 1,
            BARCODE = 2,// Comienzo y fin de la acción de escanear el código de barras
            SCAN = 3,// Comienzo y fin del escaneo del otro lado del DNI
            DATA_ENTRY = 4,// Comienzo y fin de la grabación de las imágenes a la base de datos
            DBSAVE = 5,// Comienzo y fin de la grabación de las imágenes a la base de datos
            WS = 6,// Comienzo y fin del envío al WS
        };

        #endregion

        #region Constructors
        static Logger()
        {
            XmlConfigurator.Configure();
            // Los mensajes preconfigurados
            customMsgs.Add((int)customCodes.APP, "aplicación");
            customMsgs.Add((int)customCodes.CLIENT, "atención al cliente");
            customMsgs.Add((int)customCodes.BARCODE, "escáneo del código de barras");
            customMsgs.Add((int)customCodes.SCAN, "escáneo de los demas datos");
            customMsgs.Add((int)customCodes.DATA_ENTRY, "carga de datos manual");
            customMsgs.Add((int)customCodes.DBSAVE, "grabación en la base de datos");
            customMsgs.Add((int)customCodes.WS, "comunicación con el WS");
        }
        #endregion

        #region DNIMethods
        public static void Debug(object msg)
        {
            logger.Debug(msg);
        }

        public static void Error(object msg)
        {
            logger.Error(msg);
        }

        public static void Fatal(object msg)
        {
            logger.Fatal(msg);
        }

        public static void Info(object msg)
        {
            logger.Info(msg);
        }

        public static void Warn(object msg)
        {
            logger.Warn(msg);
        }

        public static void Trace(this ILog log, string message, Exception exception)
        {
            log.Logger.Log(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType,
                traceLevel, message, exception);
        }

        public static void Trace(this ILog log, string message)
        {
            log.Trace(message, null);
        }


        #endregion

    }
}
