﻿using System.Windows;
using AperturaEnCaja.Singleton;
using System.Data.Odbc;
using System;
using Microsoft.Win32;
using System.IO;
using System.Text.RegularExpressions;

namespace AperturaEnCaja
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            loadCombo();
        }

        private void loadCombo()
        {
            /*
            cmbCajas.Items.Clear();
            cmbCajas.Items.Add("Todas las cajas");
            OdbcCommand cmd = DB.openConn().CreateCommand();
            string strSQL = "select distinct id_caja from imgs order by id_caja";
            cmd.CommandText = strSQL;
            OdbcDataReader rdr = cmd.ExecuteReader();
            while (rdr.Read())
            {
                cmbCajas.Items.Add(rdr.GetString(0));
            }
            cmbCajas.SelectedIndex = 0;*/
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private OdbcDataReader readerImgs(bool withImg)
        {
            string strSQL = "SELECT [ts],[id_caja],[celular],[email],[importe],[dni],[apellidos],[nombres],[sexo],[nacionalidad],[fnacimiento],[username],[ccnum]";
            if (withImg)
                strSQL += ",[img1],[img2]";
            strSQL += "from [imgs] where 1=1";

            /*
            if (cmbCajas.SelectedIndex != 0)
            {
                strSQL += "and id_caja = ? ";
                cmd.Parameters.Add("id_caja", OdbcType.VarChar).Value = cmbCajas.SelectedValue.ToString().Trim();
            }
            */

            OdbcDataReader rdr = null;
            OdbcCommand cmd = DB.openConn().CreateCommand();

            if (txtDNI.Text != null && txtDNI.Text != "")
            {
                strSQL += "and dni = ? ";
                cmd.Parameters.Add("dni", OdbcType.VarChar).Value = txtDNI.Text;
            }

            if (fechaDesde.Text != null)
            {
                strSQL += "and ts >= ? ";
                cmd.Parameters.Add("fdesde", OdbcType.DateTime).Value = fechaDesde.Value;
            }

            if (fechaHasta.Text != null)
            {
                strSQL += "and ts <= ? ";
                DateTime fh = (DateTime)fechaHasta.Value;
                fh = fh.AddDays(1);
                cmd.Parameters.Add("fdesde", OdbcType.DateTime).Value = fh;
            }

            strSQL += " and dni is not null and img1 is not null and img2 is not null order by [id]";
            cmd.CommandText = strSQL;
            rdr = cmd.ExecuteReader();

            return rdr;
        }

        private void btnExcel_Click(object sender, RoutedEventArgs e)
        {

            SaveFileDialog fileSave = new SaveFileDialog();
            fileSave.Filter = "Archivos CSV (*.csv) |*.csv";
            fileSave.DefaultExt = "csv";
            string ls = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ListSeparator;
            if (fileSave.ShowDialog() == true)
            {
                try
                {
                    prgData.Visibility = Visibility.Visible;
                    StreamWriter fileWr = new StreamWriter(fileSave.FileName);
                    OdbcDataReader rdr = readerImgs(false);
                    fileWr.Write("\"Fecha y hora\""); fileWr.Write(ls);
                    fileWr.Write("\"Caja\""); fileWr.Write(ls);
                    fileWr.Write("\"Operador\""); fileWr.Write(ls);
                    fileWr.Write("\"Nro. DNI\""); fileWr.Write(ls);
                    fileWr.Write("\"Apellidos\""); fileWr.Write(ls);
                    fileWr.Write("\"Nombres\""); fileWr.Write(ls);
                    fileWr.Write("\"Sexo\""); fileWr.Write(ls);
                    fileWr.Write("\"Nacionalidad\""); fileWr.Write(ls);
                    fileWr.Write("\"Fecha de nacimiento\""); fileWr.Write(ls);
                    fileWr.Write("\"Celular\""); fileWr.Write(ls);
                    fileWr.Write("\"Email\""); fileWr.Write(ls);
                    fileWr.Write("\"Importe\""); fileWr.Write(ls);
                    fileWr.Write("\"Resultado de la operación\""); fileWr.Write(ls);
                    fileWr.WriteLine();
                    while (rdr.Read())
                    {
                        string dtStr = rdr["ts"].ToString();
                        DateTime dt = DateTime.Parse(dtStr);
                        fileWr.Write("\"" + dt.ToString("dd/MM/yyyy HH:mm:ss") + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["id_caja"].ToString() + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["username"].ToString() + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["dni"].ToString() + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["apellidos"].ToString() + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["nombres"].ToString() + "\""); fileWr.Write(ls);
                        if (rdr["sexo"].ToString() == "")
                            fileWr.Write("\"Femenino\"");
                        else
                            fileWr.Write("\"" + rdr["sexo"].ToString() + "\"");
                        fileWr.Write(ls);
                        if (rdr["nacionalidad"].ToString() == "")
                            fileWr.Write("\"\"");
                        else
                            fileWr.Write("\"" + rdr["nacionalidad"].ToString() + "\"");
                        fileWr.Write(ls);
                        
                        if (rdr["fnacimiento"].ToString() == "")
                            fileWr.Write("\"\"");
                        else
                        {
                            dtStr = rdr["fnacimiento"].ToString();
                            dt = DateTime.Parse(dtStr);
                            fileWr.Write("\"" + dt.ToString("dd/MM/yyyy") + "\"");
                        }

                        fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["celular"].ToString() + "\""); fileWr.Write(ls);
                        fileWr.Write("\"" + rdr["email"].ToString() + "\""); fileWr.Write(ls);
                        fileWr.Write(rdr["importe"].ToString()); fileWr.Write(ls);
                        string pan = rdr["ccnum"].ToString();
                        long n;
                        if ( long.TryParse(pan, out n) )
                            fileWr.Write("\"PAN GENERADO OK\"");
                        else
                            fileWr.Write("\"" + pan + "\"");
                        fileWr.Write(ls);

                        fileWr.WriteLine();
                    }
                    fileWr.Close();
                    MessageBox.Show("Exportación finalizada", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    prgData.Visibility = Visibility.Hidden;
                }

            }

        }

        private void btnPdf_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog fileSave = new SaveFileDialog();
            fileSave.Filter = "Archivos PDF (*.pdf) |*.pdf";
            fileSave.DefaultExt = "pdf";
            if (fileSave.ShowDialog() == true)
            {
                try
                {
                    prgData.Visibility = Visibility.Visible;
                    OdbcDataReader rdr = readerImgs(true);
                    PDFFile pdfFile = new PDFFile();
                    pdfFile.createFile(fileSave.FileName,rdr);
                    MessageBox.Show("Exportación finalizada", "Info", MessageBoxButton.OK, MessageBoxImage.Information);
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                }
                finally
                {
                    prgData.Visibility = Visibility.Hidden;
                }
            }
        }

        private void txtDNI_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[0-9]+");
            e.Handled = !regex.IsMatch(e.Text);
        }
    }
}
