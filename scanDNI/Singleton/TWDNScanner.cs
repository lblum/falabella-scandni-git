﻿using System;
using TwainDotNet; 
using TwainDotNet.Wpf; 
using System.Windows; 
using System.ComponentModel; 
 
namespace scanDNI.Singleton
{
    class TWDNScanner
    {
        private static object syncRoot = new Object();
        private static TWDNScanner instance = null;

        private static ScanSettings scnSettings = new ScanSettings
        {
            UseDocumentFeeder = false,
            ShowTwainUI = false,
            ShowProgressIndicatorUI = true,
            UseDuplex = false,
            Resolution = new ResolutionSettings{
                Dpi = 150,
                ColourSetting = ColourSetting.Colour
            },
            
            ShouldTransferAllPages = true,
            Rotation = new RotationSettings
            {
                AutomaticRotate = true,
                AutomaticBorderDetection = true
            }
        };

        private TWDNScanner()
        {
        }

        private static TWDNScanner Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new TWDNScanner();
                }
                return instance;
            }

        }

        private static Window owner = null;
        private static Twain mainTwain = null;

        public static Window Owner
        {
            get
            {
                return owner;
            }
            set
            {
                // Seteos 
                scnSettings.UseDocumentFeeder = Properties.Settings.Default.UseDocumentFeeder;
                scnSettings.ShowTwainUI = Properties.Settings.Default.ShowTwainUI;
                scnSettings.ShowProgressIndicatorUI = Properties.Settings.Default.ShowProgressIndicatorUI;
                scnSettings.UseDuplex = Properties.Settings.Default.UseDuplex;
                scnSettings.ShouldTransferAllPages = Properties.Settings.Default.ShouldTransferAllPages;
                scnSettings.ShowTwainUI = Properties.Settings.Default.ShowTwainUI;

                scnSettings.Rotation.AutomaticRotate = Properties.Settings.Default.AutomaticRotate;
                scnSettings.Rotation.AutomaticBorderDetection = Properties.Settings.Default.AutomaticBorderDetection;
                scnSettings.Resolution = new ResolutionSettings
                {
                    Dpi = Properties.Settings.Default.Dpi,
                    ColourSetting = Properties.Settings.Default.Color ? ColourSetting.Colour : ColourSetting.GreyScale
                };

                // A donde van los eventos 
                owner = value;

                // El objeto de interface 
                try
                {
                    mainTwain = new Twain(new WpfWindowMessageHook(owner));
                }
                catch (Exception ex)
                {
                    Logger.Error("Problemas con el scanner:" + ex.Message);
                }

            }
        }

        public static Twain Device
        {
            get
            {
                return mainTwain;
            }
        }

        public static bool DevicePresent
        {
            get
            {
                if (mainTwain == null)
                    return false;
                bool retVal = false;
                try
                {
                    // Los dispositivos conectados 
                    var sourceList = mainTwain.SourceNames;
                    if (sourceList.Count > 0)
                        retVal = true;
                }
                catch (Exception ex)
                {
                    retVal = false;
                }
                return retVal;
            }
        }

        public delegate void GoFuncDel();
        public GoFuncDel GoFunc;
        public delegate void BackFuncDel();
        public BackFuncDel BackFunc;

        public static EventHandler<TransferImageEventArgs> OnTransferImage;
        public static EventHandler<ScanningCompleteEventArgs> OnScanComplete = null;


        private static BackgroundWorker mainWorker = null;
        public static void startScan()
        {
            if (OnTransferImage == null || OnScanComplete == null)
                throw new Exception("No se han completado las funciones de acceso al scanner");

            mainTwain.ScanningComplete -= OnScanComplete;
            mainTwain.ScanningComplete += OnScanComplete;

            mainTwain.TransferImage -= OnTransferImage;
            mainTwain.TransferImage += OnTransferImage;

            string devSrc = mainTwain.SourceNames[0];
            mainTwain.SelectSource(devSrc);
            mainTwain.StartScanning(scnSettings);

        }

        private static void workerScan(object sender, DoWorkEventArgs e)
        {
            object[] scanData = (object[])(e.Argument);
            ScanSettings scanSet = (ScanSettings)scanData[0];
            Twain scanDev = (Twain)scanData[1];
            string devSrc = scanDev.SourceNames[0];
            scanDev.SelectSource(devSrc);
            scanDev.StartScanning(scanSet);
        }

        private static void workerScanCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            // TODO: Housekeeping 
        }
    }
}