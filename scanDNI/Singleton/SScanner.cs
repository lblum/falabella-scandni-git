﻿using Saraff.Twain;
using System;

namespace scanDNI.Singleton
{
    class SScanner
    {
        private static readonly Twain32 _twain32 = new Twain32();

        public static bool DevicePresent
        {
            get
            {
                bool retVal = false;
                try
                {
                    _twain32.OpenDSM();
                    retVal = _twain32.SourcesCount > 0;
                    _twain32.CloseDSM();
                }
                catch (Exception ex)
                {
                    return false;
                }

                return retVal;
            }
        }
    }
}
