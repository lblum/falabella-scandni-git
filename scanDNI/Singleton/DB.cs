﻿using System;
using System.Data;
using System.Data.Odbc;

namespace scanDNI.Singleton
{
    public class DB
    {
        private static object syncRoot = new Object();
        private static OdbcConnection dbConn = null;
        private static OdbcConnection duplConn = null;

        private DB()
        {
        }

        private static OdbcConnection getConn()
        {
            if (dbConn == null)
            {
                lock (syncRoot)
                {
                    if (dbConn == null)
                    {
                        dbConn = new OdbcConnection(Properties.Settings.Default.dbDSN);
                    }
                }
            }
            return dbConn;
        }

        public static OdbcConnection openConn()
        {
            if (getConn().State != ConnectionState.Open)
                getConn().Open();
            return getConn();
        }

        public static OdbcConnection duplicateConn()
        {
            if (duplConn == null)
            {
                lock (syncRoot)
                {
                    if (duplConn == null)
                    {
                        duplConn = new OdbcConnection(Properties.Settings.Default.dbDSN);
                    }
                }
            }
            return duplConn;
        }

        public static void closeConn()
        {
            if (getConn().State != ConnectionState.Closed)
                getConn().Close();
            if (dbConn != null)
            {
                lock (syncRoot)
                {
                    if (dbConn != null)
                    {
                        dbConn = null;
                    }
                }
            }
        }

        public static void closeDuplConn()
        {
            if (duplConn.State != ConnectionState.Closed)
                duplConn.Close();
            if (duplConn != null)
            {
                lock (syncRoot)
                {
                    if (duplConn != null)
                    {
                        duplConn = null;
                    }
                }
            }
        }

        public static string getLastDNI()
        {
            OdbcCommand cmd = openConn().CreateCommand();
            cmd.CommandText = "select lastDNI from testData";
            OdbcDataReader rdr = cmd.ExecuteReader();
            rdr.Read();
            int lastDNI = rdr.GetInt32(0);
            rdr.Close();
            cmd.CommandText = "update testData set lastDNI = lastDNI + 1";
            cmd.ExecuteNonQuery();
            return lastDNI.ToString();
        }


        public static void saveTrace(int outgoing,string txt)
        {
            OdbcCommand cmd = openConn().CreateCommand();
            cmd.CommandText = "insert into wstrace(id_caja,outgoing,msg) values(?,?,?)";
            cmd.Parameters.Add("id_caja", OdbcType.VarChar).Value = Utils.IdCaja;
            cmd.Parameters.Add("outgoing", OdbcType.Int).Value = outgoing;
            cmd.Parameters.Add("txt", OdbcType.Text).Value = txt;
            cmd.ExecuteNonQuery();
        }

        public static bool dniValido(string nroDNI)
        {
            bool retVal;
            try
            {

                OdbcCommand cmd = openConn().CreateCommand();
                cmd.CommandText = "select dbo.dniValido(?)";
                cmd.Parameters.Add("nroDNI", OdbcType.VarChar).Value = nroDNI;

                object retObj = cmd.ExecuteScalar();

                retVal = ((int)retObj) == 1;

            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                retVal = true;
            }

            return retVal;
        }

        private static string getParam(string paramName, string paramDefault)
        {
            string retVal = paramDefault;
            OdbcCommand cmd = null;
            OdbcDataReader rdr = null; ;
            try
            {
                cmd = openConn().CreateCommand();
                cmd.CommandText = "select paramVal from param where paramName=?";
                cmd.Parameters.Add("paramName", OdbcType.VarChar).Value = paramName;
                rdr = cmd.ExecuteReader();
                if (rdr.Read())
                {
                    retVal = rdr.GetString(0);
                }
            } catch (Exception ex)
            {
                Logger.Error(ex.Message);
            }
            finally
            {
                rdr.Close();
            }
            return retVal;
        }

        public static float maxImporte()
        {
            float retVal = float.Parse(getParam("maxImp", "0"));
            return retVal;
        }

        public static void logMsg(int evtCode , string msg , string extra_msg)
        {
            try
            {
                string strSQL = "insert into [dbo].[perf_log]\n";
                strSQL += "([id_caja]\n";
                strSQL += ",[username]\n";
                strSQL += ",[evento]\n";
                strSQL += ",[msg]\n";
                strSQL += ",[extra_msg])\n";
                strSQL += "values (?,?,?,?,?)";
                OdbcCommand cmd = openConn().CreateCommand();
                cmd.CommandText = strSQL;
                cmd.Parameters.Add("id_caja", OdbcType.VarChar).Value = Utils.IdCaja;
                cmd.Parameters.Add("username", OdbcType.VarChar).Value = Utils.UserName;
                cmd.Parameters.Add("evento", OdbcType.Int).Value = evtCode;
                cmd.Parameters.Add("msg", OdbcType.VarChar).Value = msg;
                cmd.Parameters.Add("extra_msg", OdbcType.VarChar).Value = extra_msg;
                cmd.ExecuteNonQuery();

            } catch (Exception ex)
            {
                Logger.Error(ex);
            }
        }
    }
}
