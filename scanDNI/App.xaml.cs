﻿using scanDNI.BarCode;
using scanDNI.Singleton;
using System;
using System.Drawing;
using System.Windows;

namespace scanDNI
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public App() 
        {
            
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);
            Application.Current.MainWindow = new MainWindow();
            Application.Current.MainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            Logger.endApp();
            base.OnExit(e);
        }

    }
}
