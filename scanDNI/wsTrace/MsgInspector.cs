﻿using scanDNI.Singleton;
using System;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Dispatcher;

namespace scanDNI.wsTrace
{
    public class MsgInspector : IClientMessageInspector
    {
        public void AfterReceiveReply(ref Message reply, object correlationState)
        {
            string txt = reply.ToString();
            try
            {
                //DB.saveTrace(0, txt);
                //Logger.Debug(txt);
            }
            catch (Exception ex)
            {
                Logger.Error("Error al logear el mesaje de retorno:" + txt);
                Logger.Error(ex);
            }

        }

        public object BeforeSendRequest(ref Message request, IClientChannel channel)
        {
            string txt = request.ToString();
            try
            {
                //B.saveTrace(1, txt);
                Logger.Debug(txt);
            }
            catch (Exception ex)
            {
                Logger.Error("Error al logear el mesaje de salida:" + txt);
                Logger.Error(ex);
            }
            return null;
        }
    }
}
