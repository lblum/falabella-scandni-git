﻿namespace scanDNI.DNI
{
	class DNINuevo : DNI
	{
		public DNINuevo(string serialData)
			: base(serialData)
		{
		}

		protected override string getNumero() { return getDataByPos(4); }
		protected override string getTipo() { return getDataByPos(5); }
		protected override string getApellidos() { return getDataByPos(1); }
		protected override string getNombres() { return getDataByPos(2); }
		protected override string getFechaNacimiento() { return getDataByPos(6); }
		public override string getSexo() { return getDataByPos(3); }

	}
}
