﻿using Microsoft.Win32;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using scanDNI.BarCode;
using System.ComponentModel;
using scanDNI.Singleton;
using System.Globalization;
using System.Data.Odbc;
using scanDNI.remoteWS;
using scanDNI.wsTrace;
using Saraff.Twain;
using System.Text.RegularExpressions;
using System.Net.Mail;


namespace scanDNI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public string currTaskID = null;
        private BackgroundWorker mainWorker;
        private Twain32 _twain32;
        private bool devicePresent = false;
        
        private bool twDN = true;

        public MainWindow()
        {
            InitializeComponent();
        }

        protected bool scanOK = true;
        protected string scanTxt = "";

        private void OnAcquireError(object sender, Twain32.AcquireErrorEventArgs e)
        {
            scanOK = false;
            if (e.Exception.ReturnCode == TwRC.Cancel)
            {
                scanTxt = "Operación cancelada por el usuario";
                Logger.Info(scanTxt);
            }
            else if (e.Exception.ReturnCode == TwRC.Success)
            {
                scanTxt = e.Exception.Message;
                Logger.Error(scanTxt);
            }
            else
            {
                scanTxt = "Error en el acceso al scanner";
                Logger.Error(scanTxt);
            }
        }

        private void OnScanComplete(object sender, Twain32.EndXferEventArgs e)
        {

            taskArea.Busy.Visibility = Visibility.Hidden;

            imageDest.Source = Decoder.GetImageStream(e.Image);
            if (imageDest == imgBarCode)
            {
                try
                {
                    decodeImg();
                    scanOK = true;
                }
                catch (Exception ex)
                {
                    scanTxt = ex.Message;
                    scanOK = false;
                }
            }
        }

        public Image imageDest = null;
        public void getBarCodeImage()
        {
            Logger.startBarCode();
            dniNumero.Text =
            dniApellidos.Text =
            dniNombres.Text =
            dniSexo.Text =
            dniNacionalidad.Text =
            dniFechaNacimiento.Text =
            dniCelular.Text =
            dniEmail.Text =
            dniImporte.Text = "";
            taskArea.CurrTaskID = "barCodeScan";
            taskArea.TaskText = "Coloque el DNI en el scanner con el código de barras hacia abajo";
            taskArea.Busy.Visibility = Visibility.Hidden;
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            imageDest = imgBarCode;

            if ((devicePresent || Properties.Settings.Default.IsInProd) && !Properties.Settings.Default.testDNI)
            {
                taskArea.GoFunc = scanImage;
            }
            else
            {
                taskArea.GoFunc = loadImage;
            }
        }

        public void scanImage()
        {
            taskArea.TaskText = "Escaneando ...";
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.Busy.Visibility = Visibility.Visible;
            try
            {
                if (twDN)
                {
                    scanOK = false;
                    TWDNScanner.startScan();
                }
                else
                {
                    _twain32.Acquire();
                    if (!scanOK)
                        throw new Exception(scanTxt);

                    // Paso al próximo
                    if (imageDest == imgBarCode)
                        getImgOtro();
                    else
                        getDataEntry();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, ex.GetType().Name, MessageBoxButton.OK, MessageBoxImage.Error);
                if (imageDest == imgBarCode)
                    getBarCodeImage();
                else
                    getImgOtro();
                return;

            }

        }

        public void loadImage()
        {
            try
            {
                OpenFileDialog opf = new OpenFileDialog();

                if (opf.ShowDialog() == true)
                {
                    BitmapImage src = new BitmapImage();
                    src.BeginInit();
                    src.UriSource = new Uri(opf.FileName);
                    src.CacheOption = BitmapCacheOption.OnLoad;
                    src.EndInit();
                    imageDest.Source = src;
                    if (imageDest == imgBarCode)
                        decodeImg();
                }
                else
                {
                    throw new Exception("Finalizada por el usuario");
                }
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            if (imageDest == imgBarCode)
                getImgOtro();
            else
                getDataEntry();
        }

        public void getImgOtro()
        {
            Logger.startScan();
            taskArea.CurrTaskID = "imgScan";
            taskArea.TaskText = "Dele la vuelta al DNI";
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnBack.Visibility = Visibility.Visible;
            taskArea.BackFunc = getBarCodeImage;
            taskArea.Busy.Visibility = Visibility.Hidden;

            imageDest = imgOtro;

            if ((devicePresent || Properties.Settings.Default.IsInProd) && !Properties.Settings.Default.testDNI)
            {
                taskArea.GoFunc = scanImage;
            }
            else
            {
                taskArea.GoFunc = loadImage;
            }
        }

        public void getDataEntry()
        {
            Logger.startDataEntry();
            taskArea.CurrTaskID = "dataEntry";
            taskArea.TaskText = "Cargue los datos adicionales";
            taskArea.btnGo.Content = "OK";
            taskArea.btnGo.Visibility = Visibility.Visible;
            taskArea.btnBack.Visibility = Visibility.Visible;
            taskArea.BackFunc = getImgOtro;
            taskArea.Busy.Visibility = Visibility.Hidden;
            dniCelular.IsReadOnly = false;
            dniEmail.IsReadOnly = false;
            dniImporte.IsReadOnly = false;
            dniCelular.IsEnabled = true;
            dniEmail.IsEnabled = true;
            dniImporte.IsEnabled = true;
            gridData.IsEnabled = true;
            dniCelular.IsEnabled = true;
            dniEmail.IsEnabled = true;

            txtCajero.Text = Utils.UserName;
            if (txtCajero.Text == null || txtCajero.Text == "")
            {
                txtCajero.Focus();
                txtCajero.IsReadOnly = false;
                txtCajero.IsEnabled = true;
            }
            else
            {
                txtCajero.IsReadOnly = true;
                txtCajero.IsEnabled = false;
            }
            taskArea.GoFunc = saveToDB;
        }

        private void validarCelular(string nroCelular)
        {
            if (nroCelular.Length == 0)
            {
                dniCelular.Focus();
                throw new Exception("Debe completar el celular");
            }

            // Valido que solo haya - o dígitos
            string pat = @"^[ \-0-9]+$";
            Regex val = new Regex(pat, RegexOptions.ExplicitCapture);
            Match m = val.Match(nroCelular);
            if (!m.Success)
            {
                dniCelular.Focus();
                throw new Exception("En el celular solo se admiten números, espacios o \"-\"");
            }

            // Valido que sean 10 dígitos
            nroCelular = nroCelular.Replace("-", "").Replace(" ", "");
            if (nroCelular.Length != 10)
            {
                dniCelular.Focus();
                throw new Exception("El celular debe contener exactamente 10 dígitos");
            }
        }

        private void validarCajero(string nroCajero)
        {
            if (nroCajero.Length == 0)
            {
                txtCajero.Focus();
                throw new Exception("Debe completar el cajero");
            }

            // Valido que solo haya - o dígitos
            string pat = @"^[ \-0-9]+$";
            Regex val = new Regex(pat, RegexOptions.ExplicitCapture);
            Match m = val.Match(nroCajero);
            if (!m.Success)
            {
                txtCajero.Focus();
                throw new Exception("En el nro de cajero solo se admiten números");
            }
        }

        protected int lastDBID = -1;

        private void saveToDB()
        {
            try
            {
                // Chequeo los valores
                validarCelular(dniCelular.Text.Trim());
                validarCajero(txtCajero.Text.Trim());
                if ( !validEmail(dniEmail.Text) )
                {
                    dniEmail.Focus();
                    throw new Exception("Dirección de correo incorrrecta");
                }
                decimal monto;
                try
                {
                    string strCI = Properties.Settings.Default.CultureInfo;

                    CultureInfo ci;
                    try
                    {
                        ci = new CultureInfo(strCI);
                    }
                    catch (Exception ex1)
                    {
                        ci = CultureInfo.InvariantCulture;
                    }
                    
                    monto = decimal.Parse(dniImporte.Text, ci);
                }
                catch (Exception ex)
                {
                    dniImporte.Focus();
                    throw new Exception("El importe es incorrecto");
                }

                decimal maxImporte = (decimal) DB.maxImporte();
                if (monto > maxImporte)
                    throw new Exception(string.Format("El importe supera el máximo permitido de ${0}", maxImporte));


                Logger.startDBSave();
                taskArea.CurrTaskID = "saveImgToDB";
                taskArea.TaskText = "Grabando datos en la base";
                taskArea.btnGo.Content = "OK";
                taskArea.btnGo.Visibility = Visibility.Collapsed;
                taskArea.btnBack.Visibility = Visibility.Collapsed;
                taskArea.Busy.Visibility = Visibility.Visible;

                // Grabo 
                /*
                mainWorker = new BackgroundWorker();
                mainWorker.DoWork += workerSaveData;
                */
                object[] dniData = new object[7];
                dniData[0] = currDNI;
                dniData[1] = Decoder.getImgArray((BitmapSource)imgBarCode.Source);
                dniData[2] = Decoder.getImgArray((BitmapSource)imgOtro.Source);
                dniData[3] = dniCelular.Text;
                dniData[4] = dniEmail.Text;
                dniData[5] = monto;// decimal.Parse(dniImporte.Text, CultureInfo.InvariantCulture);
                dniData[6] = txtCajero.Text;// decimal.Parse(dniImporte.Text, CultureInfo.InvariantCulture);
                dniCelular.IsReadOnly = true;
                dniEmail.IsReadOnly = true;
                dniImporte.IsReadOnly = true;
                dniCelular.IsEnabled = false;
                dniEmail.IsEnabled = false;
                dniImporte.IsEnabled = false;
                txtCajero.IsEnabled = false;
                workerSaveData(dniData);
                /*
                mainWorker.RunWorkerCompleted += workerSaveDataCompleted;
                mainWorker.RunWorkerAsync(dniData);
                */
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
        }

        private void workerSaveData(object[] dniData)
        {
            OdbcCommand cmd = DB.openConn().CreateCommand();
            //object[] dniData = (object[])(e.Argument);
            DNI.DNI dni = (DNI.DNI)dniData[0];
            cmd.CommandText = "insert into imgs(id_caja,img1,img2,pdf_417_data,celular,email,importe,dni,apellidos,nombres,sexo,nacionalidad,fnacimiento,username)values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            //cmd.CommandText = "insert into imgs(id_caja,img1,img2,pdf_417_data,celular,email,importe,dni,apellidos,nombres,sexo,nacionalidad,username)values(?,?,?,?,?,?,?,?,?,?,?,?,?)";
            cmd.Parameters.Add("id_caja", OdbcType.VarChar).Value = Utils.IdCaja;
            cmd.Parameters.Add("img1", OdbcType.VarBinary).Value = (byte[])dniData[1];
            cmd.Parameters.Add("img2", OdbcType.VarBinary).Value = (byte[])dniData[2];
            cmd.Parameters.Add("pdf_417_data", OdbcType.VarChar).Value = ((DNI.DNI)dniData[0]).SerialData;
            cmd.Parameters.Add("celular", OdbcType.VarChar).Value = (string)dniData[3];// wnd.dniCelular.Text; ;
            cmd.Parameters.Add("email", OdbcType.VarChar).Value = (string)dniData[4];
            cmd.Parameters.Add("importe", OdbcType.Decimal).Value = (decimal)dniData[5];

            cmd.Parameters.Add("dni", OdbcType.VarChar).Value = dni.Numero;
            cmd.Parameters.Add("apellidos", OdbcType.VarChar).Value = dni.Apellidos;
            cmd.Parameters.Add("nombres", OdbcType.VarChar).Value = dni.Nombres;
            cmd.Parameters.Add("sexo", OdbcType.VarChar).Value = dni.Sexo;
            cmd.Parameters.Add("nacionalidad", OdbcType.VarChar).Value = dni.Nacionalidad;
            DateTime dt = DateTime.ParseExact(dni.FechaNacimiento, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            string strDate = dt.ToString("yyyyMMdd");
            cmd.Parameters.Add("fnacimiento", OdbcType.VarChar).Value = strDate;
            
            cmd.Parameters.Add("username", OdbcType.VarChar).Value = (string)dniData[6];

            cmd.ExecuteNonQuery();

            // Busco el nro insertado
            cmd.CommandText = "select IDENT_CURRENT('imgs') as id";
            OdbcDataReader rdr = cmd.ExecuteReader();
            rdr.Read();
            lastDBID = rdr.GetInt32(0);
            rdr.Close();

            taskArea.Busy.Visibility = Visibility.Collapsed;
            // TODO: chequear excepciones

            sendDataToWS();
        }

        public void sendDataToWS()
        {
            Logger.startWS();
            taskArea.CurrTaskID = "sendToWS";
            taskArea.TaskText = "Enviando al Web Service";
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;

            gridData.IsEnabled = false;
            taskArea.Busy.Visibility = Visibility.Visible;
            // Grabo 
            mainWorker = new BackgroundWorker();
            mainWorker.DoWork += workerSendToWS;
            object[] wsData = new object[5];
            wsData[0] = currDNI;
            wsData[1] = dniCelular.Text;
            wsData[2] = dniEmail.Text;
            wsData[3] = float.Parse(dniImporte.Text, CultureInfo.InvariantCulture);
            wsData[4] = txtCajero.Text;
            mainWorker.RunWorkerCompleted += workerSendToWSCompleted;
            mainWorker.RunWorkerAsync(wsData);
        }

        private void workerSendToWS(object sender, DoWorkEventArgs e)
        {
            try
            {

                object[] wsData = (object[])(e.Argument);
                string wsPort = Properties.Settings.Default.wsPort;
                TiendaOriginacionPtClient client = new TiendaOriginacionPtClient(wsPort);
                // Agrego el Trace
                client.Endpoint.Behaviors.Add(new MsgBehavior());

                DNI.DNI wsDNI = (DNI.DNI)wsData[0];
                string wsCelular = (string)wsData[1];
                string wsEmail = (string)wsData[2];
                string wsImporte = string.Format("{0:00000000.00}", ((float)wsData[3])).Replace(",", ".");
                string wsCajero = (string)wsData[4];
                //if (wsImporte.IndexOf("+") < 0)
                //    wsImporte = "+" + wsImporte;

                // Header
                ClientService_TYPE wsHeader = new ClientService_TYPE
                {
                    country = Country_TYPE.AR,
                    commerce = Commerce_TYPE.Falabella,
                    channel = Channel_TYPE.PuntodeVenta
                };


                // Separo los nombres
                string[] auxNombres = wsDNI.Nombres.Split(' ');
                string auxNombre1 = "";
                string auxNombre2 = "";
                if (auxNombres.Length >= 1)
                {
                    auxNombre1 = auxNombres[0];
                    if (auxNombres.Length >= 2)
                        auxNombre2 = auxNombres[1];
                }

                // La carga útil
                solicitarOriginacionTienda_TYPE wsPayload = new solicitarOriginacionTienda_TYPE
                {
                    tipoPolitica = "AP0",
                    tipoDocto = "D",
                    numeroIdentidad = Properties.Settings.Default.mockDNI ? DB.getLastDNI() : wsDNI.Numero,
                    apellidos = wsDNI.Apellidos,
                    nombre1 = auxNombre1,
                    nombre2 = auxNombre2,
                    fechaNacimiento = wsDNI.getFechaNacimientoAnsi(),
                    celularparticular = wsCelular,
                    emailpart = wsEmail,
                    importeSolicitado = wsImporte,
                    sexo = wsDNI.getSexo(),
                    CodigoVendedor = wsCajero
                };
                try
                {
                    Logger.Debug("Importe =>" + wsImporte);
                    e.Result = client.TiendaOriginacionOp(wsHeader, wsPayload);
                }
                catch (Exception ex)
                {
                    Logger.Error("Error en el web service");
                    Logger.Error(ex);
                    Logger.endWS("Error");
                    e.Result = null;
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error en el web service");
                Logger.Error(ex);
                Logger.endWS("Error");
                e.Result = null;
            }

        }

        private void workerSendToWSCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

            
            taskArea.TaskText = "Proceso finalizado";
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.Busy.Visibility = Visibility.Collapsed;

            try
            {
                if (e.Result == null)
                {
                    saveCCNum("Error en la comunicación con el WS");
                    if (MessageBox.Show("Problemas en la comunicación con el WS. Reintenta?", "Error!", MessageBoxButton.YesNo, MessageBoxImage.Warning) == MessageBoxResult.Yes)
                    {
                        sendDataToWS();
                        return;
                    }
                    else
                    {
                        taskArea.btnGo.Content = "Presione para cerrar la ventana";
                        taskArea.btnGo.Visibility = Visibility.Visible;
                        taskArea.GoFunc = endWork;
                    }
                }
                else
                {
                    // Muestro el resultado
                    solicitarOriginacionTiendaResponse_TYPE resp = (solicitarOriginacionTiendaResponse_TYPE)e.Result;
                    Logger.endWS("WS finalizado OK");
                    int codigoMotivo = Int32.Parse(resp.codigoMotivo);
                    if (codigoMotivo != 0)
                    {
                        MessageBox.Show("Dictamen: " + resp.dictamen, "Solicitud rechazada", MessageBoxButton.OK);
                        saveCCNum(resp.dictamen);
                        endWork();
                    }
                    else
                    {
                        string[] arrData = resp.dictamen.Split('@');
                        if (arrData.Length < 2)
                        {
                            saveCCNum("Número de tarjeta no informado:" + resp.motivo);
                            MessageBox.Show(resp.motivo, "Número de tarjeta no informado", MessageBoxButton.OK);
                            endWork();

                        }
                        else
                        {
                            string nroTarjeta = arrData[1].Trim();
                            taskArea.txtTarjeta.Text = nroTarjeta;
                            //taskArea.txtTarjeta.Visibility = Visibility.Visible;
                            taskArea.txtTarjeta.Focus();
                            taskArea.txtTarjeta.SelectAll();
                            taskArea.txtTarjeta.Copy();
                            taskArea.txtTask.Content = "Cuenta creada exitosamente";
                            taskArea.btnGo.Content = "Presione para cerrar la ventana";
                            taskArea.btnGo.Visibility = Visibility.Visible;
                            sendMail();
                            saveCCNum("000000000000");// nroTarjeta);
                            taskArea.GoFunc = endWork;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error en el web service");
                Logger.Error(ex);
                Logger.endWS("Error");
            }
        }

        protected void saveCCNum(string nroTarjeta)
        {
            OdbcCommand cmd = DB.openConn().CreateCommand();
            cmd.CommandText = "update imgs set ccnum = ? where id = ?";
            cmd.Parameters.Add("ccnum", OdbcType.VarChar).Value = nroTarjeta;
            cmd.Parameters.Add("id", OdbcType.Int).Value = lastDBID;
            cmd.ExecuteNonQuery();
        }

        public DNI.DNI currDNI = null;

        public void decodeImg()
        {

            currDNI = Decoder.decodeImg((BitmapSource)imageDest.Source);
            if (currDNI == null)
                throw new Exception("No se puede procesar el código de barras en la imagen");
            gridData.Visibility = Visibility.Collapsed;
            dniNumero.Text = currDNI.Numero;
            dniApellidos.Text = currDNI.Apellidos;
            dniNombres.Text = currDNI.Nombres;
            dniSexo.Text = currDNI.Sexo;
            dniNacionalidad.Text = currDNI.Nacionalidad;
            dniFechaNacimiento.Text = currDNI.FechaNacimiento;
            // Chequeo que el DNI esté en la tabla de preaprobados
            if (!DB.dniValido(currDNI.Numero))
                throw new Exception("El DNI no está en la tabla de preaprobados");

            if (Properties.Settings.Default.IsInProd)
            {
                dniCelular.Text = "";
                dniEmail.Text = "";
                dniImporte.Text = "";
            }
            else
            {
                dniCelular.Text = "11 4444-4444";
                dniEmail.Text = "correo@dominio.com";
                dniImporte.Text = "1234.56";
            }
            this.gridData.Visibility = Visibility.Visible;
        }

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            //e.Cancel = true;
            //endWork();
        }

        public void startWork()
        {
            /*
            try
            {
                DB.openConn();
            }
            catch (Exception ex)
            {
                Logger.Error(ex);
                MessageBox.Show("Imposible conectarse con la base de datos", "error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            */
            try
            {
                
                //imgBarCode.Source = Decoder.GetImageStream(Properties.Resources.im_invisible_user);
                //imgOtro.Source = Decoder.GetImageStream(Properties.Resources.im_invisible_user);
                dniNumero.Text =
                dniApellidos.Text =
                dniNombres.Text =
                dniSexo.Text =
                dniNacionalidad.Text =
                dniFechaNacimiento.Text =
                dniCelular.Text =
                dniEmail.Text =
                dniImporte.Text = "";
                setScanner();
            }
            catch (Exception ex)
            {
                Logger.Error(ex.Message);
                if (Properties.Settings.Default.IsInProd)
                {
                    MessageBox.Show("Imposible conectarse con el scanner", "error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
            getBarCodeImage();
        }

        private void setScanner()
        {
            if (twDN)
            {
                // Interfaz TWainDotNet
                TWDNScanner.Owner = this;
                if ( (devicePresent = TWDNScanner.DevicePresent))
                {
                    // Los eventos
                    TWDNScanner.OnScanComplete = OnScanningComplete;
                    TWDNScanner.OnTransferImage = OnTransferImage;
                }
                
            }
            else
            {
                // Interfaz Saraff
                _twain32 = new Twain32();
                _twain32.OpenDSM();
                devicePresent = true;
                if (_twain32.SourcesCount < 1)
                {
                    devicePresent = false;
                    return;
                }
                _twain32.SetDefaultSource(0);
                foreach (TwCap t in Enum.GetValues(typeof(TwCap)))
                {
                    try
                    {
                        TwQC cap = _twain32.IsCapSupported(t);
                        Logger.Info(string.Format("{0}", t.ToString()));
                    }
                    catch (Exception ex)
                    {

                    }
                }
                _twain32.ShowUI = Properties.Settings.Default.ShowTwainUI;

                if ((_twain32.IsCapSupported(TwCap.AutomaticRotate) & TwQC.Set) != 0)
                    _twain32.SetCap(TwCap.AutomaticRotate, true);

                //if ((tw.IsCapSupported(TwCap.) & TwQC.Set) != 0)
                //  tw.SetCap(TwCap.AutomaticRotate, true);


                _twain32.EndXfer += OnScanComplete;
                _twain32.AcquireError += OnAcquireError;
            }

        }

        private void OnTransferImage(object sender, TwainDotNet.TransferImageEventArgs e)
        {
            if (e.Image != null)
            {
                imageDest.Source = Decoder.GetImageStream(e.Image);

                taskArea.Busy.Visibility = Visibility.Hidden;
                scanOK = true;
                scanTxt = "";
                if (imageDest == imgBarCode)
                {
                    try
                    {
                        decodeImg();
                        scanOK = true;
                    }
                    catch (Exception ex)
                    {
                        scanTxt = ex.Message;
                        scanOK = false;
                    }
                }
                if (!scanOK)
                    throw new Exception(scanTxt);

                // Paso al próximo
                if (imageDest == imgBarCode)
                    getImgOtro();
                else
                    getDataEntry();
            } else
            {
                if (imageDest == imgBarCode)
                    getBarCodeImage();
                else
                    getImgOtro();
            }

        }

        private void OnScanningComplete(object sender, TwainDotNet.ScanningCompleteEventArgs e)
        {
            Exception ex = e.Exception; ;

            if (ex == null & !scanOK)
                ex = new Exception("Operación cancelada por el usuario");
            if (ex != null)
            {
                MessageBox.Show(ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);

                if (imageDest == imgBarCode)
                    getBarCodeImage();
                else
                    getImgOtro();
            }
        }

        public void endWork()
        {
            Visibility = Visibility.Hidden;
            DB.closeConn();

            if (_twain32 != null)
                try
                {
                    _twain32.Dispose();
                }
                catch (Exception ex)
                {
                    // TODO: LOG

                }
            Application.Current.Shutdown();
        }

        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            endWork();
        }

        private bool validCell(string text)
        {
            Regex regex = new Regex("[ 0-9-]+");
            if (!regex.IsMatch(text))
                return false;

            text = text.Replace("-", "").Replace(" ", "");
            if (text.Length > 10)
                return false;
            return true;
        }

        private bool validEmail(string text)
        {
            // Sin acentos ni diéresis ni ñ
            if (System.Text.Encoding.UTF8.GetByteCount(text) != text.Length)
                return false;
            Regex regex = new Regex(@"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
            if (!regex.IsMatch(text))
                return false;

            // Una sola arroba
            if (Regex.Matches(text, "@").Count > 1)
                return false;

            string inv = Properties.Settings.Default.invalidEmailChars;
            for ( int i=0; i < inv.Length; i++)
            {
                if (text.IndexOf(inv.Substring(i, 1)) > 0)
                    return false;
            }

            return true;
        }

        private bool validImporte(string text)
        {
            Regex regex = new Regex(@"[\.,0-9-]+");
            if (!regex.IsMatch(text))
                return false;

            return true;

        }

        private bool dataValidation(string text,Delegate validFunc)
        {
            bool retVal = (bool)validFunc.DynamicInvoke(text);
            return retVal;
        }

        private void pasteValidation(object sender, DataObjectPastingEventArgs e, Delegate validFunc)
        {
            if (e.DataObject.GetDataPresent(typeof(String)))
            {
                String text = (String)e.DataObject.GetData(typeof(String));
                bool retVal = (bool)validFunc.DynamicInvoke(text);
                if (!retVal)
                {
                    e.CancelCommand();
                }
            }
            else
            {
                e.CancelCommand();
            }
        }


        private void txtCajero_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !dataValidation(e.Text, new Func<string, bool>(validCell));
        }

        private void txtCajero_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            pasteValidation(sender, e, new Func<string, bool>(validCell));
        }

        private void dniCelular_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !dataValidation(e.Text, new Func<string, bool>(validCell));
        }

        private void dniCelular_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            pasteValidation(sender, e, new Func<string, bool>(validCell));
        }

        private void dniImporte_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            e.Handled = !dataValidation(e.Text, new Func<string, bool>(validImporte));
        }

        private void dniImporte_Pasting(object sender, DataObjectPastingEventArgs e)
        {
            pasteValidation(sender, e, new Func<string, bool>(validImporte));
        }

        protected void sendMail()
        {
            if (!Properties.Settings.Default.sendMail)
                return;
            try
            {
                Logger.Info("Enviando correo ...");
                SmtpClient client = new SmtpClient();
                client.Port = Properties.Settings.Default.smtpPort;
                client.Host = Properties.Settings.Default.smtpServer;
                client.EnableSsl = Properties.Settings.Default.smtpUseSSL;
                client.Timeout = 10000;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(Properties.Settings.Default.smtpUser, Properties.Settings.Default.smtpPass);

                MailMessage mm = new MailMessage(Properties.Settings.Default.sendFrom, Properties.Settings.Default.sendTo);
                //mm.BodyEncoding = E
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                mm.Subject = "Nueva tarjeta CMR";

                mm.Body = "DNI : " + dniNumero.Text + "\n"
                        + "Nombre(s) : " + dniNombres + "\n"
                        + "Apellido(s) : " + dniApellidos + "\n"
                        + "ID Caja : " + Utils.IdCaja + "\n"
                        + "ID Cajero : " + txtCajero.Text;

                client.Send(mm);
                Logger.Info("Correo enviado");

            }
            catch (Exception ex)
            {
                Logger.Error("Error al enviar correo: " + ex.Message);
            }
        }

        private void Window_ContentRendered(object sender, EventArgs e)
        {
            Logger.startApp();
            twDN = Properties.Settings.Default.useTWDN;

            startWork();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            taskArea.Busy.Visibility = Visibility.Visible;
            taskArea.btnGo.Visibility = Visibility.Collapsed;
            taskArea.btnBack.Visibility = Visibility.Collapsed;
            taskArea.txtTask.Content = "Un momento, por favor ...";
        }
    }
}
