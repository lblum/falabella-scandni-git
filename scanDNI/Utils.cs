﻿using scanDNI.Singleton;
using System;
using System.IO;


namespace scanDNI
{
    public class Utils
    {
        public static string IdCaja
        {
            get
            {
                //
                string retVal = Properties.Settings.Default.idCaja;
                try
                {
                    string[] fList = Directory.GetFiles(Properties.Settings.Default.ownPath, Properties.Settings.Default.ownExt);
                    if (fList.Length > 0)
                    {
                        string fName = Path.GetFileNameWithoutExtension(fList[0]);
                        string[] fParts = fName.Split('.');
                        retVal = fParts[1].Substring(3);
                    }
                }
                catch (Exception ex)
                {
                    //retVal = Properties.Settings.Default.idCaja;
                }


                return retVal;
            }
        }

        private static string FuserName = null;

        private static string getLasUserName(string fileName)
        {
            string userName = "";

            string line;
            using (var file = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (var reader = new StreamReader(file))
            {           
                while ((line = reader.ReadLine()) != null)
                {
                    if (line.IndexOf("SET_CASHIER_NUMBER_EVENT(") > 0)
                    {
                        int indx = line.IndexOf("^b");
                        if (indx > 0)
                        {
                            indx += 7;
                            userName = line.Substring(indx, 5);
                        }
                        Console.WriteLine(line);
                    }

                }
                file.Close();
            }


            return userName;
        }
            
        
        public static string UserName
        {
            get
            {
                if (FuserName == null)
                {
                    try
                    {
                        FuserName = getLasUserName(Properties.Settings.Default.posDBGFile);
                        if (FuserName == "")
                        {
                            Logger.Info("No se encuentra cajero en "+ Properties.Settings.Default.posDBGFile);

                            FuserName = getLasUserName(Properties.Settings.Default.posDBGFile + ".bak");
                            if (FuserName == "")
                            {
                                Logger.Info("No se encuentra cajero en " + Properties.Settings.Default.posDBGFile + ".bak");
                            }
                        }
                        Logger.Info("Cajero:" + FuserName);

                    }
                    catch(Exception ex)
                    {
                        Logger.Error("Error al buscar el usuario:" + ex.Message);
                        FuserName = "";
                    }
                }

                return FuserName;
            }
        }
    }
}
