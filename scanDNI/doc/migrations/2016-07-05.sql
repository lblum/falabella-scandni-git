﻿CREATE DATABASE [scan-dni]
GO
USE [scan-dni]
GO
CREATE TABLE [dbo].[imgs](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[ts] [datetime] NULL CONSTRAINT [DF_imgs_ts]  DEFAULT (getdate()),
	[id_caja] [varchar](max) NULL,
	[img1] [image] NULL,
	[img2] [image] NULL,
	[pdf_417_data] [varchar](max) NULL,
	[celular] [varchar](max) NULL,
	[email] [varchar](max) NULL,
	[importe] [decimal](18, 2) NULL,
	primary key (id)
)
GO
CREATE TABLE [dbo].[wstrace](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_caja] [varchar](max) NOT NULL,
	[ts] [datetime] NOT NULL CONSTRAINT [DF_wstrace_ts]  DEFAULT (getdate()),
	[outgoing] [int] NOT NULL,
	[msg] [text] NOT NULL,
	primary key (id)
)
GO
