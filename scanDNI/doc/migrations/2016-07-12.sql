﻿alter table imgs add dni varchar(10);
alter table imgs add apellidos varchar(max);
alter table imgs add nombres varchar(max);
alter table imgs add sexo varchar(max);
alter table imgs add nacionalidad varchar(max);
alter table imgs add fnacimiento date;
alter table imgs add username varchar(max);


CREATE TABLE [dbo].[perf_log](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_caja] [varchar](max) NOT NULL,
	[username] [varchar](max) NOT NULL,
	[cuando] [datetime] NOT NULL CONSTRAINT [DF_log_cuando]  DEFAULT (getdate()),
	[evento] [int] NOT NULL,
	[msg] [varchar](max) NULL,
	[extra_msg] [varchar](max) NULL,
	PRIMARY KEY ([id])
)
go

create table ccnum(
	[id] [int] IDENTITY(1,1) NOT NULL,
	[id_caja] [varchar](max) NOT NULL,
	[username] [varchar](max) NOT NULL,
	[pan] varchar(max) not null
)
go
