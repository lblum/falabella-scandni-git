﻿create function dniValido
(
	@nroDNI varchar(8)
)
returns int
as
begin
	declare @nDni int;

	/*
	select @nDni = count(*)
		from tabla
		where nroDni = @nroDNI;

	if @nDNI = 0
		return 0;

	*/
	return 1;
end;
go
create table [dbo].[param](
	[paramName] [varchar](50) NOT NULL,
	[paramVal] [varchar](max) NOT NULL,
	[paramDescr] [varchar](max) NULL,
	constraint [pk_param] primary key
	(
		[paramName]
	)
)
go
insert [dbo].[param] ([paramName], [paramVal], [paramDescr]) values (N'maxImp', N'5000', N'Importe máximo para la primera compra')
go
