﻿using System;
using System.Data;
using System.Data.Odbc;

namespace testApp.Singleton
{
    public class DB
    {
        private static object syncRoot = new Object();
        private static OdbcConnection dbConn = null;
        private static OdbcConnection duplConn = null;

        private DB()
        {
        }

        private static OdbcConnection getConn()
        {
            if (dbConn == null)
            {
                lock (syncRoot)
                {
                    if (dbConn == null)
                    {
                        dbConn = new OdbcConnection(Properties.Default.dbDSN);
                    }
                }
            }
            return dbConn;
        }

        public static OdbcConnection openConn()
        {
            if (getConn().State != ConnectionState.Open)
                getConn().Open();
            return getConn();
        }

        public static OdbcConnection duplicateConn()
        {
            if (duplConn == null)
            {
                lock (syncRoot)
                {
                    if (duplConn == null)
                    {
                        dbConn = new OdbcConnection(Properties.Default.dbDSN);
                    }
                }
            }
            return duplConn;
        }

        public static void closeConn()
        {
            if (getConn().State != ConnectionState.Closed)
                getConn().Close();
            if (dbConn != null)
            {
                lock (syncRoot)
                {
                    if (dbConn != null)
                    {
                        dbConn = null;
                    }
                }
            }
        }

        public static void closeDuplConn()
        {
            if (duplConn.State != ConnectionState.Closed)
                duplConn.Close();
            if (duplConn != null)
            {
                lock (syncRoot)
                {
                    if (duplConn != null)
                    {
                        duplConn = null;
                    }
                }
            }
        }

    }
}
