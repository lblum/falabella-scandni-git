﻿
using System.Data.Odbc;
using System;
using testApp.Singleton;
using System.IO;

namespace testApp
{
    class Program
    {
        static void Main(string[] args)
        {
            checkData(args[0]);

            Console.Write("Presione una tecla para continuar ... ");
            Console.ReadKey();
            Console.WriteLine();
        }


        protected static void checkData(string fName)
        {

            OdbcCommand cmd = DB.openConn().CreateCommand();

            cmd.CommandText = File.ReadAllText(fName);
            cmd.ExecuteNonQuery();

            DB.closeConn();

        }
    }
}
